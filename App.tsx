import { StatusBar } from 'expo-status-bar';
import React, { useEffect, useState } from 'react';
import { Button, StyleSheet, Text, View } from 'react-native';
import * as Notifications from 'expo-notifications';
import * as Permissions from 'expo-permissions'; // add it if u want to use in IOS


interface token{
 
}

Notifications.setNotificationHandler({
  handleNotification: async () => {
    return {
      shouldShowAlert: true,
      shouldPlaySound: true,
      shouldSetBadge: false
    };
  }
});



export default function App() {

  const [pushToken, setPushToken] = useState<token>();

  useEffect(() => {

    const BackgroundSubcription = Notifications.addNotificationResponseReceivedListener(reponse => {
      console.log(reponse);
    })


    const ForegroundSubcription = Notifications.addNotificationReceivedListener(notifications => {
      console.log(notifications);
    });

    return () => {
      ForegroundSubcription.remove();
      BackgroundSubcription.remove();
    }

  }, [])

  useEffect(() => {
    Permissions.getAsync(Permissions.NOTIFICATIONS)
      .then((statusObj) => {
        if (statusObj.status !== 'granted') {
          return Permissions.askAsync(Permissions.NOTIFICATIONS)
        }
        return statusObj;
      }).then((statusObj) => {
        if (statusObj.status !== 'granted') {
          throw new Error('Permission is not granted!');

        }
      })
      .then(() => {
        console.log("Get Token");
        return Notifications.getExpoPushTokenAsync();
      })
      .then(response => {
        console.log(response);
        const token = response.data;
        setPushToken(token);
      })
      .catch(err => { 
        console.log(err);
        return null; 
      })
  })

  const triggerNotifiation = () => {
    // Notifications.scheduleNotificationAsync({
    //   content: {
    //     title: 'My first local notification',
    //     body: 'This is the first local notification we are sending',
    //     data: { mySpecialData: 'hello there !' }
    //   },
    //   trigger: {
    //     seconds: 5
    //   }
    // });

    fetch("https://exp.host/--/api/v2/push/send",{
      method: 'POST',
        headers:{
          Accept: 'application/json',
          'Accept-Encoding': 'gzip, deflate',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          to: pushToken,
          data: {extraData: 'Some Data'},
          title: 'Sent via the app',
          body: 'This push notification was sent the via the app'
        })
    });
  }

  return (
    <View style={styles.container}>
      <Button title="Trigger Notifications" onPress={triggerNotifiation} />
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
